package org.trueafrican.monitoringtool.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.AdvertDao;
import org.trueafrican.monitoringtool.dao.AdvertStationMappingDao;
import org.trueafrican.monitoringtool.model.Advert;
import org.trueafrican.monitoringtool.model.AdvertStationMapping;
import org.trueafrican.monitoringtool.model.Airing;
import org.trueafrican.monitoringtool.service.AdvertsService;

@Service("advertsService")
@Transactional
public class AdvertsServiceImpl  implements AdvertsService {

	@Autowired
    private AdvertDao advertDao;
	
	@Autowired
    private AdvertStationMappingDao advertStationMappingDao;
	
	public AdvertsServiceImpl() {}
	
    public AdvertsServiceImpl(AdvertDao advertDao) {
        this.advertDao = advertDao;
    }
	
	public void deleteAdvert(Advert advert) {
		advertDao.deleteAdvert(advert);
	}

	public List<Advert> findAllAdverts() {
		List<Advert> adverts = advertDao.findAllAdverts();
		List<Advert> newList = new ArrayList<Advert>();
		 for (Advert advert : adverts) {
			 Advert advertDTO = new Advert(advert);
			 newList.add(advertDTO);
		 }
		 return newList;
	}

	public List<Advert> findAllAdverts(String startDate, String endDate, String advertName) {
		List<Advert> adverts = null;
		try {
			adverts = advertDao.findAllAdverts(startDate, endDate, advertName);
		} catch (ParseException e) {
			
		}
		List<Advert> newList = new ArrayList<Advert>();
		 for (Advert advert : adverts) {
			 Advert advertDto = new Advert(advert);
			 newList.add(advertDto);
		 }
		 return newList;
	}

	public Advert findByAdvertId(Long advertId) {
		Advert advert = advertDao.findByAdvertId(advertId);
		if (advert != null) {
			return new Advert(advert);
		}
		return null;
	}

	public Advert saveAdvert(Advert advert) {
		Advert advertDto = advertDao.saveAdvert(advert);
		if (advertDto != null) {
			return new Advert(advertDto);
		}
		return null;
	}

	public Advert updateAdvert(Advert advert) {
		Advert advertDto = advertDao.updateAdvert(advert);
		if (advertDto != null) {
			return new Advert(advertDto);
		}
		return null;
	}

	public List<Advert> findAllAdverts(String startDate, String endDate, Long companyId) {
		List<Advert> adverts = null;
		try {
			adverts = advertDao.findAllAdverts(startDate, endDate, companyId);
		} catch (ParseException e) {
			
		}
		List<Advert> newList = new ArrayList<Advert>();
		 for (Advert advert : adverts) {
			 Advert advertDto = new Advert(advert);
			 newList.add(advertDto);
		 }
		 return newList;
	}

	public List<Advert> findAllAdverts(Long companyId) {
		List<Advert> adverts = advertDao.findAllAdverts(companyId);
		
		List<Advert> newList = new ArrayList<Advert>();
		 for (Advert advert : adverts) {
			 Advert advertDto = new Advert(advert);
			 newList.add(advertDto);
		 }
		 return newList;
	}

	public void deleteAiring(Airing airing) {
		advertDao.deleteAiring(airing);
	}

	public List<Airing> findAllAirings(Long advertId) {
		return advertDao.findAllAirings(advertId);
	}

	public Airing saveAiring(Airing airing) {
		return advertDao.saveAiring(airing);
	}

	public void updateAiring(Airing airing) {
		advertDao.updateAiring(airing);
	}

	public Airing findByAiringId(Long airingId) {
		return advertDao.findByAiringId(airingId);
	}

	public List<AdvertStationMapping> findAdvertsMappedToStation(Long stationId) {
		return advertStationMappingDao.findAdvertsMappedToStation(stationId);
	}

	public List<AdvertStationMapping> findStationsMappedToAdvert(Long advertId) {
		return advertStationMappingDao.findStationsMappedToAdvert(advertId);
	}

	public void saveAdvertStationMapping(List<AdvertStationMapping> mappings) {
		advertStationMappingDao.saveAdvertStationMapping(mappings);
	}

	public boolean updateAdvert(Long advertId, String audioFileName) {
		return advertDao.updateAdvert(advertId, audioFileName);
	}
}