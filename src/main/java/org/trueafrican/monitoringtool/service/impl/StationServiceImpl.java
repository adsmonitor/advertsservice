package org.trueafrican.monitoringtool.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.CompanyStationMappingDao;
import org.trueafrican.monitoringtool.dao.StationDao;
import org.trueafrican.monitoringtool.dao.StreamRecordDao;
import org.trueafrican.monitoringtool.model.CompanyStationMapping;
import org.trueafrican.monitoringtool.model.Station;
import org.trueafrican.monitoringtool.model.StationCategory;
import org.trueafrican.monitoringtool.model.StreamRecord;
import org.trueafrican.monitoringtool.service.StationService;

@Service("stationService")
@Transactional
public class StationServiceImpl  implements StationService {

	@Autowired
	private StationDao stationDao;

	@Autowired
	private CompanyStationMappingDao companyStationMappingDao;

	@Autowired
	private StreamRecordDao streamRecordDao;

	public StationServiceImpl() {}

	public StationServiceImpl(StationDao stationDao) {
		this.stationDao = stationDao;
	}

	public void deleteStation(Station station) {
		stationDao.deleteStation(station);
	}

	public List<Station> findAllStations() {
		List<Station> stations = stationDao.findAllStations();
		List<Station> newList = new ArrayList<Station>();
		for (Station station : stations) {
			Station stationDTO = new Station(station);
			newList.add(stationDTO);
		}
		return newList;
	}

	public List<Station> findAllStations(String startDate, String endDate, String stationName) {
		List<Station> stations = null;
		try {
			stations = stationDao.findAllStations(startDate, endDate, stationName);
		} catch (ParseException e) {

		}
		List<Station> newList = new ArrayList<Station>();
		for (Station station : stations) {
			Station stationDto = new Station(station);
			newList.add(stationDto);
		}
		return newList;
	}

	public Station findByStationId(Long stationId) {
		Station station = stationDao.findByStationId(stationId);
		if (station != null) {
			return new Station(station);
		}
		return null;
	}

	public Station saveStation(Station station) {
		Station stationDto = stationDao.saveStation(station);
		if (stationDto != null) {
			return new Station(stationDto);
		}
		return null;
	}

	public Station updateStation(Station station) {
		Station stationDto = stationDao.updateStation(station);
		if (stationDto != null) {
			return new Station(stationDto);
		}
		return null;
	}

	public List<CompanyStationMapping> findStationsMappedToCompany(Long companyId) {
		return companyStationMappingDao.findStationsMappedToCompany(companyId);
	}

	public void saveCompanyStationMapping(List<CompanyStationMapping> mappings) {
		companyStationMappingDao.saveCompanyStationMapping(mappings);
	}

	public List<Station> findAllStations(StationCategory stationCategory) {
		return stationDao.findAllStations(stationCategory);
	}

	public void deleteStreamRecord(StreamRecord streamRecord) {
		streamRecordDao.deleteStreamRecord(streamRecord);
	}

	public List<StreamRecord> findAllStreamRecordings() {
		return streamRecordDao.findAllStreamRecordings();
	}

	public List<StreamRecord> findAllStreamRecordings(Long stationId) {
		return streamRecordDao.findAllStreamRecordings(stationId);
	}

	public List<StreamRecord> findAllStreamRecordings(String startDate, String endDate, Long stationId) {
		List<StreamRecord> stations = null;

		try {
			stations = streamRecordDao.findAllStreamRecordings(startDate, endDate, stationId);
		} catch (ParseException e) {}
		return stations;
	}

	public StreamRecord findByStreamRecordId(Long recordId) {
		return streamRecordDao.findByStreamRecordId(recordId);
	}

	public StreamRecord saveStreamRecord(StreamRecord streamRecord) {
		return streamRecordDao.saveStreamRecord(streamRecord);
	}

	public StreamRecord updateStreamRecord(StreamRecord streamRecord) {
		return streamRecordDao.updateStreamRecord(streamRecord);
	}
}