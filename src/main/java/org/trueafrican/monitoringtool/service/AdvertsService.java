package org.trueafrican.monitoringtool.service;

import java.util.List;

import org.trueafrican.monitoringtool.model.Advert;
import org.trueafrican.monitoringtool.model.AdvertStationMapping;
import org.trueafrican.monitoringtool.model.Airing;

public interface AdvertsService {

	Advert saveAdvert(Advert advert);
    
    List<Advert> findAllAdverts();
    
    List<Advert> findAllAdverts(String startDate, String endDate, String advertName);
    
    List<Advert> findAllAdverts(String startDate, String endDate, Long companyId);
    
    List<Advert> findAllAdverts(Long companyId);
     
    void deleteAdvert(Advert advert);
     
    Advert findByAdvertId(Long advertId);
     
    Advert updateAdvert(Advert advert);
    
    boolean updateAdvert(Long advertId, String audioFileName);
    
    List<Airing> findAllAirings(Long advertId);
    
    void deleteAiring(Airing airing);
    
    Airing saveAiring(Airing airing);
    
    void updateAiring(Airing airing);
    
    Airing findByAiringId(Long airingId);
    
    void saveAdvertStationMapping(List<AdvertStationMapping> mappings);
	
	List<AdvertStationMapping> findStationsMappedToAdvert(Long advertId);
	
	List<AdvertStationMapping> findAdvertsMappedToStation(Long stationId);
}