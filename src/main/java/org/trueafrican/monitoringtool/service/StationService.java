package org.trueafrican.monitoringtool.service;

import java.util.List;

import org.trueafrican.monitoringtool.model.CompanyStationMapping;
import org.trueafrican.monitoringtool.model.Station;
import org.trueafrican.monitoringtool.model.StationCategory;
import org.trueafrican.monitoringtool.model.StreamRecord;

public interface StationService {

	Station saveStation(Station station);
    
    List<Station> findAllStations();
    
    List<Station> findAllStations(StationCategory stationCategory);
    
    List<Station> findAllStations(String startDate, String endDate, String stationName);
     
    void deleteStation(Station station);
     
    Station findByStationId(Long stationId);
     
    Station updateStation(Station station);
    
    void saveCompanyStationMapping(List<CompanyStationMapping> mappings);
	
	List<CompanyStationMapping> findStationsMappedToCompany(Long companyId);
	
	StreamRecord saveStreamRecord(StreamRecord streamRecord);
    
    List<StreamRecord> findAllStreamRecordings();
    
    List<StreamRecord> findAllStreamRecordings(Long stationId);
    
    List<StreamRecord> findAllStreamRecordings(String startDate, String endDate, Long stationId);
     
    void deleteStreamRecord(StreamRecord streamRecord);
     
    StreamRecord findByStreamRecordId(Long streamRecordId);
     
    StreamRecord updateStreamRecord(StreamRecord streamRecord);
}