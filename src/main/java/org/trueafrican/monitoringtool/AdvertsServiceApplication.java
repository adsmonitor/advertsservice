package org.trueafrican.monitoringtool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:app-config.xml")
//@EnableDiscoveryClient
public class AdvertsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdvertsServiceApplication.class, args);
	}
}