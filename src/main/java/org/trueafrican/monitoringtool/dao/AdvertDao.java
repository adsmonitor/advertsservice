package org.trueafrican.monitoringtool.dao;

import java.text.ParseException;
import java.util.List;

import org.trueafrican.monitoringtool.model.Advert;
import org.trueafrican.monitoringtool.model.Airing;

public interface AdvertDao {
	
	Advert saveAdvert(Advert advert);
    
    List<Advert> findAllAdverts();
    
    List<Advert> findAllAdverts(String startDate, String endDate, String advertName)  throws ParseException;
    
    List<Advert> findAllAdverts(String startDate, String endDate, Long companyId)  throws ParseException;
    
    List<Advert> findAllAdverts(Long companyId);
     
    void deleteAdvert(Advert advert);
     
    Advert findByAdvertId(Long advertId);
     
    Advert updateAdvert(Advert advert);
    
    boolean updateAdvert(Long advertId, String audioFileName);
    
    List<Airing> findAllAirings(Long advertId);
    
    void deleteAiring(Airing airing);
    
    Airing saveAiring(Airing airing);
    
    void updateAiring(Airing airing);
    
    Airing findByAiringId(Long airingId);
}