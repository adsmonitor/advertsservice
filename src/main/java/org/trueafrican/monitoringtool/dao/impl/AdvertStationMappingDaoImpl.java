package org.trueafrican.monitoringtool.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.AbstractDao;
import org.trueafrican.monitoringtool.dao.AdvertStationMappingDao;
import org.trueafrican.monitoringtool.model.AdvertStationMapping;
import org.trueafrican.monitoringtool.model.CompanyStationMapping;

@Transactional
@Repository("advertStationMappingDao")
public class AdvertStationMappingDaoImpl extends AbstractDao implements AdvertStationMappingDao {

	@SuppressWarnings("unchecked")
	public List<AdvertStationMapping> findAdvertsMappedToStation(Long stationId) {
		Criteria criteria = getSession().createCriteria(CompanyStationMapping.class);
		criteria.add(Restrictions.eq("stationId", stationId));
		
		List mappings = criteria.list();
		if (mappings == null) {
			return new ArrayList<AdvertStationMapping>();
		} 
        return (List<AdvertStationMapping>) mappings;
	}

	@SuppressWarnings("unchecked")
	public List<AdvertStationMapping> findStationsMappedToAdvert(Long advertId) {
		Criteria criteria = getSession().createCriteria(AdvertStationMapping.class);
		criteria.add(Restrictions.eq("advertId", advertId));
		
		List mappings = criteria.list();
		if (mappings == null) {
			return new ArrayList<AdvertStationMapping>();
		} 
        return (List<AdvertStationMapping>) mappings;
	}

	public void saveAdvertStationMapping(List<AdvertStationMapping> mappings) {
		for (AdvertStationMapping mapping : mappings)
			save(mapping);
	}
}