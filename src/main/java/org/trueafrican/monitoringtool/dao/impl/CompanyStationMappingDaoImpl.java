package org.trueafrican.monitoringtool.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.AbstractDao;
import org.trueafrican.monitoringtool.dao.CompanyStationMappingDao;
import org.trueafrican.monitoringtool.model.CompanyStationMapping;

@Transactional
@Repository("companyStationMappingDao")
public class CompanyStationMappingDaoImpl extends AbstractDao implements CompanyStationMappingDao {

	@SuppressWarnings("unchecked")
	public List<CompanyStationMapping> findStationsMappedToCompany(Long companyId) {
		Criteria criteria = getSession().createCriteria(CompanyStationMapping.class);
		criteria.add(Restrictions.eq("companyId", companyId));
		
		List adverts = criteria.list();
		if (adverts == null) {
			return new ArrayList<CompanyStationMapping>();
		} 
        return (List<CompanyStationMapping>) adverts;
	}

	public void saveCompanyStationMapping(List<CompanyStationMapping> mappings) {
		for (CompanyStationMapping mapping : mappings)
			save(mapping);
	}
}