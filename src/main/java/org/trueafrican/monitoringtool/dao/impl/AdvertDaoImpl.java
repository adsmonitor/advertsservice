package org.trueafrican.monitoringtool.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.AbstractDao;
import org.trueafrican.monitoringtool.dao.AdvertDao;
import org.trueafrican.monitoringtool.model.Advert;
import org.trueafrican.monitoringtool.model.Airing;

@Transactional
@Repository("advertDao")
public class AdvertDaoImpl extends AbstractDao implements AdvertDao {

	public void deleteAdvert(Advert advert) {
		delete(advert);
	}

	@SuppressWarnings("unchecked")
	public List<Advert> findAllAdverts() {
		Criteria criteria = getSession().createCriteria(Advert.class);
		List<Advert> adverts = (List<Advert>) criteria.list();
        return adverts;
	}

	@SuppressWarnings("unchecked")
	public List<Advert> findAllAdverts(String startDate, String endDate, String advertName) throws ParseException {
		Criteria criteria = getSession().createCriteria(Advert.class);
		
		if (startDate != null && endDate != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date startdate = formatter.parse(startDate);
			Date enddate = formatter.parse(endDate);
			criteria.add(Restrictions.between("dateCreated", startdate, enddate));
		} else {
			if (startDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date startdate = formatter.parse(startDate);
				criteria.add(Restrictions.ge("dateCreated", startdate));
			}
			
			if (endDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date enddate = formatter.parse(endDate);
				criteria.add(Restrictions.le("dateCreated", enddate));
			}
		}
		
		if (advertName != null) {
			criteria.add(Restrictions.like("advertName", "%" + advertName + "%"));
		}
		
		List adverts = criteria.list();
		if (adverts == null) {
			return new ArrayList<Advert>();
		} 
        return (List<Advert>) adverts;
	}

	public Advert findByAdvertId(Long advertId) {
		Criteria criteria = getSession().createCriteria(Advert.class);
		
		if (advertId != null) {
			criteria.add(Restrictions.eq("advertId", advertId));
		}
        return (Advert)criteria.uniqueResult();
	}

	public Advert saveAdvert(Advert advert) {
		save(advert);
		Query query = getSession().createQuery("FROM Advert ORDER BY advertId DESC");
		query.setMaxResults(1);
        return (Advert)query.uniqueResult();
	}

	public Advert updateAdvert(Advert advert) {
		saveOrUpdate(advert);
		return findByAdvertId(advert.getAdvertId());
	}

	@SuppressWarnings("unchecked")
	public List<Advert> findAllAdverts(String startDate, String endDate, Long companyId) throws ParseException {
		Criteria criteria = getSession().createCriteria(Advert.class);
		
		if (startDate != null && endDate != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date startdate = formatter.parse(startDate);
			Date enddate = formatter.parse(endDate);
			criteria.add(Restrictions.between("dateCreated", startdate, enddate));
		} else {
			if (startDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date startdate = formatter.parse(startDate);
				criteria.add(Restrictions.ge("dateCreated", startdate));
			}
			
			if (endDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date enddate = formatter.parse(endDate);
				criteria.add(Restrictions.le("dateCreated", enddate));
			}
		}
		
		if (companyId != null) {
			criteria.add(Restrictions.eq("companyId", companyId));
		}
		
		List adverts = criteria.list();
		if (adverts == null) {
			return new ArrayList<Advert>();
		} 
        return (List<Advert>) adverts;
	}

	@SuppressWarnings("unchecked")
	public List<Advert> findAllAdverts(Long companyId) {
		Criteria criteria = getSession().createCriteria(Advert.class);
		criteria.add(Restrictions.eq("companyId", companyId));
		
		List adverts = criteria.list();
		if (adverts == null) {
			return new ArrayList<Advert>();
		} 
        return (List<Advert>) adverts;
	}

	public void deleteAiring(Airing airing) {
		delete(airing);
	}

	@SuppressWarnings("unchecked")
	public List<Airing> findAllAirings(Long advertId) {
		Criteria criteria = getSession().createCriteria(Airing.class);
		criteria.add(Restrictions.eq("advertId", advertId));
		List<Airing> airings = (List<Airing>) criteria.list();
        return airings;
	}

	public Airing saveAiring(Airing airing) {
		save(airing);
		Query query = getSession().createQuery("FROM Airing ORDER BY airingId DESC");
		query.setMaxResults(1);
        return (Airing)query.uniqueResult();
	}

	public void updateAiring(Airing airing) {
		saveOrUpdate(airing);
	}

	public Airing findByAiringId(Long airingId) {
		Criteria criteria = getSession().createCriteria(Airing.class);
		
		if (airingId != null) {
			criteria.add(Restrictions.eq("airingId", airingId));
		}
        return (Airing)criteria.uniqueResult();
	}

	public boolean updateAdvert(Long advertId, String audioFileName) {
		String query = "UPDATE Advert SET ";
		boolean result = false;
		int updatedEntities = 0;
		query = query + "audioFileName = :audioFileName";
		query = query + " WHERE advertId = :advertId";

		updatedEntities = getSession().createQuery(query)
		.setString("audioFileName", audioFileName)
		.setLong("advertId", advertId)
		.executeUpdate();

		if (updatedEntities > 0)
			result = true;
		return result;
	}
}