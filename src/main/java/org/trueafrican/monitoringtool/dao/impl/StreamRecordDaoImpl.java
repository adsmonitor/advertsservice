package org.trueafrican.monitoringtool.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.AbstractDao;
import org.trueafrican.monitoringtool.dao.StreamRecordDao;
import org.trueafrican.monitoringtool.model.StreamRecord;

@Transactional
@Repository("streamRecordDao")
public class StreamRecordDaoImpl extends AbstractDao implements StreamRecordDao {

	public void deleteStreamRecord(StreamRecord streamRecord) {
		delete(streamRecord);
	}

	@SuppressWarnings("unchecked")
	public List<StreamRecord> findAllStreamRecordings() {
		Criteria criteria = getSession().createCriteria(StreamRecord.class);
		List<StreamRecord> streamRecords = (List<StreamRecord>) criteria.list();
        return streamRecords;
	}

	@SuppressWarnings("unchecked")
	public List<StreamRecord> findAllStreamRecordings(String startDate, String endDate, Long stationId) throws ParseException {
		Criteria criteria = getSession().createCriteria(StreamRecord.class);
		
		if (startDate != null && endDate != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date startdate = formatter.parse(startDate);
			Date enddate = formatter.parse(endDate);
			criteria.add(Restrictions.between("dateCreated", startdate, enddate));
		} else {
			if (startDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date startdate = formatter.parse(startDate);
				criteria.add(Restrictions.ge("dateCreated", startdate));
			}
			
			if (endDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date enddate = formatter.parse(endDate);
				criteria.add(Restrictions.le("dateCreated", enddate));
			}
		}
		
		if (stationId != null) {
			criteria.add(Restrictions.eq("stationId", stationId));
		}
		
		List streamRecords = criteria.list();
		if (streamRecords == null) {
			return new ArrayList<StreamRecord>();
		} 
        return (List<StreamRecord>) streamRecords;
	}

	public StreamRecord findByStreamRecordId(Long recordId) {
		Criteria criteria = getSession().createCriteria(StreamRecord.class);
		
		if (recordId != null) {
			criteria.add(Restrictions.eq("recordId", recordId));
		}
        return (StreamRecord)criteria.uniqueResult();
	}

	public StreamRecord saveStreamRecord(StreamRecord streamRecord) {
		save(streamRecord);
		Query query = getSession().createQuery("FROM StreamRecord ORDER BY recordId DESC");
		query.setMaxResults(1);
        return (StreamRecord)query.uniqueResult();
	}

	public StreamRecord updateStreamRecord(StreamRecord streamRecord) {
		saveOrUpdate(streamRecord);
		return findByStreamRecordId(streamRecord.getRecordId());
	}

	@SuppressWarnings("unchecked")
	public List<StreamRecord> findAllStreamRecordings(Long stationId) {
		Criteria criteria = getSession().createCriteria(StreamRecord.class);
		
		if (stationId != null) {
			criteria.add(Restrictions.eq("stationId", stationId));
		}
		
		List streamRecords = criteria.list();
		if (streamRecords == null) {
			return new ArrayList<StreamRecord>();
		} 
        return (List<StreamRecord>) streamRecords;
	}
}