package org.trueafrican.monitoringtool.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.AbstractDao;
import org.trueafrican.monitoringtool.dao.StationDao;
import org.trueafrican.monitoringtool.model.Station;
import org.trueafrican.monitoringtool.model.StationCategory;

@Transactional
@Repository("stationDao")
public class StationDaoImpl extends AbstractDao implements StationDao {

	public void deleteStation(Station station) {
		delete(station);
	}

	@SuppressWarnings("unchecked")
	public List<Station> findAllStations() {
		Criteria criteria = getSession().createCriteria(Station.class);
		List<Station> stations = (List<Station>) criteria.list();
        return stations;
	}

	@SuppressWarnings("unchecked")
	public List<Station> findAllStations(String startDate, String endDate, String stationName) throws ParseException {
		Criteria criteria = getSession().createCriteria(Station.class);
		
		if (startDate != null && endDate != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date startdate = formatter.parse(startDate);
			Date enddate = formatter.parse(endDate);
			criteria.add(Restrictions.between("dateCreated", startdate, enddate));
		} else {
			if (startDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date startdate = formatter.parse(startDate);
				criteria.add(Restrictions.ge("dateCreated", startdate));
			}
			
			if (endDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date enddate = formatter.parse(endDate);
				criteria.add(Restrictions.le("dateCreated", enddate));
			}
		}
		
		if (stationName != null) {
			criteria.add(Restrictions.like("stationName", "%" + stationName + "%"));
		}
		
		List stations = criteria.list();
		if (stations == null) {
			return new ArrayList<Station>();
		} 
        return (List<Station>) stations;
	}

	public Station findByStationId(Long stationId) {
		Criteria criteria = getSession().createCriteria(Station.class);
		
		if (stationId != null) {
			criteria.add(Restrictions.eq("stationId", stationId));
		}
        return (Station)criteria.uniqueResult();
	}

	public Station saveStation(Station station) {
		save(station);
		Query query = getSession().createQuery("FROM Station ORDER BY stationId DESC");
		query.setMaxResults(1);
        return (Station)query.uniqueResult();
	}

	public Station updateStation(Station station) {
		saveOrUpdate(station);
		return findByStationId(station.getStationId());
	}

	@SuppressWarnings("unchecked")
	public List<Station> findAllStations(StationCategory stationCategory) {
		Criteria criteria = getSession().createCriteria(Station.class);
		
		if (stationCategory != null) {
			criteria.add(Restrictions.eq("stationCategory", stationCategory));
		}
		
		List<Station> stations = (List<Station>) criteria.list();
        return stations;
	}
}