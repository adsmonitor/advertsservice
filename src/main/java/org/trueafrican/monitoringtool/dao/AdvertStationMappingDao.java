package org.trueafrican.monitoringtool.dao;

import java.util.List;

import org.trueafrican.monitoringtool.model.AdvertStationMapping;

public interface AdvertStationMappingDao {
	
	void saveAdvertStationMapping(List<AdvertStationMapping> mappings);
	
	List<AdvertStationMapping> findStationsMappedToAdvert(Long advertId);
	
	List<AdvertStationMapping> findAdvertsMappedToStation(Long stationId);
}