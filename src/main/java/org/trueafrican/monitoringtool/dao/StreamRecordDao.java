package org.trueafrican.monitoringtool.dao;

import java.text.ParseException;
import java.util.List;

import org.trueafrican.monitoringtool.model.StreamRecord;

public interface StreamRecordDao {
	
	StreamRecord saveStreamRecord(StreamRecord streamRecord);
    
    List<StreamRecord> findAllStreamRecordings();
    
    List<StreamRecord> findAllStreamRecordings(Long stationId);
    
    List<StreamRecord> findAllStreamRecordings(String startDate, String endDate, Long stationId) throws ParseException;
     
    void deleteStreamRecord(StreamRecord streamRecord);
     
    StreamRecord findByStreamRecordId(Long recordId);
     
    StreamRecord updateStreamRecord(StreamRecord streamRecord);
}