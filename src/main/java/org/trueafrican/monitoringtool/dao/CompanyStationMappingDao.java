package org.trueafrican.monitoringtool.dao;

import java.util.List;

import org.trueafrican.monitoringtool.model.CompanyStationMapping;

public interface CompanyStationMappingDao {
	
	void saveCompanyStationMapping(List<CompanyStationMapping> mappings);
	
	List<CompanyStationMapping> findStationsMappedToCompany(Long companyId);
}