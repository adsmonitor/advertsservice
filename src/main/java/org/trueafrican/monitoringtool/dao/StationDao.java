package org.trueafrican.monitoringtool.dao;

import java.text.ParseException;
import java.util.List;

import org.trueafrican.monitoringtool.model.Station;
import org.trueafrican.monitoringtool.model.StationCategory;

public interface StationDao {
	
	Station saveStation(Station station);
    
    List<Station> findAllStations();
    
    List<Station> findAllStations(StationCategory stationCategory);
    
    List<Station> findAllStations(String startDate, String endDate, String stationName)  throws ParseException;
     
    void deleteStation(Station station);
     
    Station findByStationId(Long stationId);
     
    Station updateStation(Station station);
}