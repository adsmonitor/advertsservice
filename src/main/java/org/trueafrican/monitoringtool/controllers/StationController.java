package org.trueafrican.monitoringtool.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.trueafrican.monitoringtool.model.CompanyStationMapping;
import org.trueafrican.monitoringtool.model.Response;
import org.trueafrican.monitoringtool.model.Station;
import org.trueafrican.monitoringtool.model.StationCategory;
import org.trueafrican.monitoringtool.model.StreamRecord;
import org.trueafrican.monitoringtool.service.StationService;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class StationController {
	
	private static final String hostUrl = "http://localhost:8080";
	
	@Autowired
	StationService stationService;
	
    public StationController() {}
	
    public StationController(StationService stationService) {
        this.stationService = stationService;
    }
 
    @DeleteMapping("/station/{id}")
	public ResponseEntity<Response> deleteStation(@PathVariable(value = "id") Long stationId) {
		Station station = stationService.findByStationId(stationId);
	    if(station == null) {
	    	return ResponseEntity.ok(new Response("Station to be deleted could not be found", "Failed"));
	    }
	    
	    stationService.deleteStation(station);
	    return ResponseEntity.ok(new Response("Station has been deleted successfully", "Success"));
	}
    
    @RequestMapping(value = "/stations", method = RequestMethod.GET)
	public @ResponseBody List<Station> findAllStations() {
		return stationService.findAllStations();
	}

    @RequestMapping(value = "/rd_stations", method = RequestMethod.GET)
	public @ResponseBody List<Station> findAllRDStations() {
		return stationService.findAllStations(StationCategory.RADIO);
	}
    
    @RequestMapping(value = "/tv_stations", method = RequestMethod.GET)
	public @ResponseBody List<Station> findAllTVStations() {
		return stationService.findAllStations(StationCategory.TELEVISION);
	}

    @GetMapping("/stations/{startDate}/{endDate}/{stationName}")
	public List<Station> findAllStations(@PathVariable(value = "startDate") String startDate, @PathVariable(value = "endDate") String endDate, @PathVariable(value = "companyName") String stationName) {
		return stationService.findAllStations(startDate, endDate, stationName);
	}

    @GetMapping("/station/{stationId}")
	public Station findByStationId(@PathVariable(value = "stationId") Long stationId) {
		return stationService.findByStationId(stationId);
	}
    
    @PostMapping("/addStation")
    @CrossOrigin(origins = hostUrl)
	public ResponseEntity<Response> saveCompany(@RequestBody String stationInfo) throws JsonParseException, IOException {
    	stationInfo = java.net.URLDecoder.decode(stationInfo, "UTF-8");
    	stationInfo = stationInfo.replaceAll("=", "");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	Station station = mapper.readValue(stationInfo, Station.class);
    	
    	station.setDateCreated(new Date());
    	station.setCreatedBy(new Long(1));
    	station = new Station(stationService.saveStation(station));
		
		if(station == null) {
            return ResponseEntity.ok(new Response("Request failed", "Failed"));
        }
		return ResponseEntity.ok(new Response("Station has been added successfully", "Success"));
	}
    
    @PostMapping("/updateStation")
    @CrossOrigin(origins = hostUrl)
	public ResponseEntity<Response> updateStation(@RequestBody String stationInfo) throws JsonParseException, IOException {
    	stationInfo = java.net.URLDecoder.decode(stationInfo, "UTF-8");
    	stationInfo = stationInfo.replaceAll("=", "");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	Station station = mapper.readValue(stationInfo, Station.class);
    	
    	station.setDateCreated(new Date());
    	station.setCreatedBy(new Long(1));
    	station = new Station(stationService.updateStation(station));
		
		if(station == null) {
            return ResponseEntity.ok(new Response("Request failed", "Failed"));
        }
		return ResponseEntity.ok(new Response("Station has been updated successfully", "Success"));
	}
    
    @GetMapping("/addCompanyStationMapping/{stations}/{companyId}")
    @CrossOrigin(origins = hostUrl)
	public ResponseEntity<Response> addCompanyStationMapping(@PathVariable(value = "stations") String stations, @PathVariable(value = "companyId") String companyId) throws JsonParseException, JsonMappingException, IOException {
    	String[] stationList = stations.split("_");
    	List<CompanyStationMapping> mappings = new ArrayList<CompanyStationMapping>();
    	
    	for (String station : stationList) {
    		CompanyStationMapping mapping = new CompanyStationMapping();
    		mapping.setCompanyId(new Long(companyId));
    		mapping.setStationId(new Long(station));
    		mapping.setDateCreated(new Date());
        	mapping.setCreatedBy(new Long(1));
        	mappings.add(mapping);
    	}
    	stationService.saveCompanyStationMapping(mappings);
		return ResponseEntity.ok(new Response("Station has been added successfully", "Success"));
	}
    
    @GetMapping("/companyStationMapping/{companyId}")
	public List<CompanyStationMapping> findCompanyStationMapping(@PathVariable(value = "companyId") Long companyId) {
		return stationService.findStationsMappedToCompany(companyId);
	}
    
    @PostMapping("/newStation")
    //@CrossOrigin(origins = hostUrl)
	public ResponseEntity<List<Station>> newStation(@RequestParam ("stationInfo") String stationInfo) throws JsonParseException, JsonMappingException, IOException {
    	ObjectMapper mapper = new ObjectMapper();
    	Station station = mapper.readValue(stationInfo, Station.class);
    	
    	station.setDateCreated(new Date());
    	station.setCreatedBy(new Long(1));
    	station = new Station(stationService.saveStation(station));
		
		if(station == null) {
            return ResponseEntity.notFound().build();
        }
	
		List<Station> stations = stationService.findAllStations();
		return ResponseEntity.ok(stations);
	}
    
    @RequestMapping(value = "/streamRecordings", method = RequestMethod.GET)
	public @ResponseBody List<StreamRecord> findAllStreamRecordings() {
		return stationService.findAllStreamRecordings();
	}
    
    @GetMapping("/streamRecordings/{stationId}")
	public List<StreamRecord> findAllStreamRecordings(@PathVariable(value = "stationId") Long stationId) {
		return stationService.findAllStreamRecordings(stationId);
	}
    
    @GetMapping("/streamRecordings/{startDate}/{endDate}/{stationId}")
	public List<StreamRecord> findAllStreamRecordings(@PathVariable(value = "startDate") String startDate, @PathVariable(value = "endDate") String endDate, @PathVariable(value = "stationId") Long stationId) {
		return stationService.findAllStreamRecordings(startDate, endDate, stationId);
	}
    
    @DeleteMapping("/deleteStreamRecord/{recordId}")
	public ResponseEntity<Response> deleteStreamRecord(@PathVariable(value = "recordId") Long recordId) {
    	StreamRecord streamRecord = stationService.findByStreamRecordId(recordId);
	    if(streamRecord == null) {
	    	return ResponseEntity.ok(new Response("Record could not be found", "Failed"));
	    }
	    
	    stationService.deleteStreamRecord(streamRecord);
	    return ResponseEntity.ok(new Response("Record has been deleted successfully", "Success"));
	}
    
    @PutMapping("/startMonitor/{stationId}")
	public ResponseEntity<Response> startMonitor(@PathVariable(value = "stationId") Long stationId) {
    	Station station = stationService.findByStationId(stationId);
    	if (!station.isStreamingStatus())
    		station.setStreamingStatus(true);
    	station = stationService.updateStation(station);
    	
    	if(station == null) {
	    	return ResponseEntity.ok(new Response("Monitor could not be set on station", "Failed"));
	    }
    	return ResponseEntity.ok(new Response("Station has been enabled for monitoring", "Success"));
	}
    
    @PutMapping("/stopMonitor/{stationId}")
	public ResponseEntity<Response> stopMonitor(@PathVariable(value = "stationId") Long stationId) {
    	Station station = stationService.findByStationId(stationId);
    	if (station.isStreamingStatus())
    		station.setStreamingStatus(false);
    	station = stationService.updateStation(station);
    	
    	if(station == null) {
	    	return ResponseEntity.ok(new Response("Monitor could not be disabled on station.", "Failed"));
	    }
    	return ResponseEntity.ok(new Response("Station is no longer being monitored.", "Success"));
	}
}