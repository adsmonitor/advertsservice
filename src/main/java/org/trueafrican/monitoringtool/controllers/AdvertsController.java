package org.trueafrican.monitoringtool.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.trueafrican.monitoringtool.model.Advert;
import org.trueafrican.monitoringtool.model.AdvertStationMapping;
import org.trueafrican.monitoringtool.model.Airing;
import org.trueafrican.monitoringtool.model.AiringStatus;
import org.trueafrican.monitoringtool.model.Response;
import org.trueafrican.monitoringtool.service.AdvertsService;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class AdvertsController {
	
	private static final String hostUrl = "http://localhost:8080";
	
	@Autowired
	AdvertsService advertsService;
	
    public AdvertsController() {}
	
    public AdvertsController(AdvertsService advertsService) {
        this.advertsService = advertsService;
    }
 
    @DeleteMapping("/del_advert/{id}")
	public ResponseEntity<Response> deleteAdvert(@PathVariable(value = "id") Long advertId) {
		Advert advert = advertsService.findByAdvertId(advertId);
	    if(advert == null) {
	        return ResponseEntity.notFound().build();
	    }
	    
	    advertsService.deleteAdvert(advert);
	    return ResponseEntity.ok(new Response("Advert has been deleted successfully", "Success"));
	}

    @GetMapping("/adverts")
    @CrossOrigin(origins = hostUrl)
	public List<Advert> findAllAdverts() {
		return advertsService.findAllAdverts();
	}

    @GetMapping("/adverts/{startDate}/{endDate}/{advertName}")
	public List<Advert> findAllAdverts(@PathVariable(value = "startDate") String startDate, @PathVariable(value = "endDate") String endDate, @PathVariable(value = "companyName") String advertName) {
		return advertsService.findAllAdverts(startDate, endDate, advertName);
	}

    @GetMapping("/advert/{advertId}")
	public Advert findByAdvertId(@PathVariable(value = "advertId") Long advertId) {
		return advertsService.findByAdvertId(advertId);
	}

    /*@PostMapping("/addAdvert")
	public ResponseEntity<Advert> saveAdvert(@Valid @RequestBody Advert advert) {
    	advert.setDateCreated(new Date());
    	advert = new Advert(advertsService.saveAdvert(advert));
		
		if(advert == null) {
            return ResponseEntity.notFound().build();
        }
	
		return ResponseEntity.ok(advert);
	}*/
    
    @PostMapping("/newAdvert")
    @CrossOrigin(origins = hostUrl)
	public ResponseEntity<Advert> saveAdvert(@RequestBody String advertInfo) throws JsonParseException, IOException {
    	advertInfo = java.net.URLDecoder.decode(advertInfo, "UTF-8");
    	advertInfo = advertInfo.replaceAll("=", "");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	Advert advert = mapper.readValue(advertInfo, Advert.class);
    	advert.setDateCreated(new Date());
    	advert.setCompanyId(new Long(1));
    	advert = new Advert(advertsService.saveAdvert(advert));
		
		if(advert == null) {
            return ResponseEntity.notFound().build();
        }
		return ResponseEntity.ok(advert);
	}

    //TODO return company object does not include the old adverts that were saved earlier..
    @PutMapping("/updateAdvert/{advertId}")
	public ResponseEntity<Advert> updateAdvert(@PathVariable(value = "advertId") Long advertId, @Valid @RequestBody Advert updatedAdvert) {
        if(advertsService.findByAdvertId(advertId) == null) {
            return ResponseEntity.notFound().build();
        }
        Advert advert = new Advert(updatedAdvert);
        advert = advertsService.updateAdvert(advert);
        
        return ResponseEntity.ok(advert);
	}
    
    @PutMapping("/updateAdvertFileName/{advertId}")
	public ResponseEntity<Response> updateAdvert(@PathVariable(value = "advertId") Long advertId, @Valid @RequestBody String fileName) {
        Response response = null;
    	if (advertsService.updateAdvert(advertId, fileName)) {
    		response = new Response("Advert has been updated successfully", "Success");
        } else {
        	response = new Response("Updating request failed", "Failed");
        }
        return ResponseEntity.ok(response);
	}
    
    
    @GetMapping("/airing/{advertId}")
	public List<Airing> findAiringByAdvertId(@PathVariable(value = "advertId") Long advertId) {
		return advertsService.findAllAirings(advertId);
	}
    
    @PutMapping("/updateAiring")
	public ResponseEntity<Airing> updateAiring(@Valid @RequestBody Airing airing) {
    	advertsService.updateAiring(airing);
		return ResponseEntity.ok(airing);
	}
    
    @PostMapping("/addAiring")
	public ResponseEntity<Airing> saveAiring(@Valid @RequestBody Airing airing) {
    	airing.setDateCreated(new Date());
    	airing.setAiringStatus(AiringStatus.NOT_AIRED);
    	airing.setCreatedBy(new Long(1));
    	airing = new Airing(advertsService.saveAiring(airing));
		
		if(airing == null) {
            return ResponseEntity.notFound().build();
        }
	
		return ResponseEntity.ok(airing);
	}
    
    @DeleteMapping("/deleteAiring/{airingId}")
	public ResponseEntity<Airing> deleteAiring(@PathVariable(value = "airingId") Long airingId) {
    	Airing airing = advertsService.findByAiringId(airingId);
	    if(airing == null) {
	        return ResponseEntity.notFound().build();
	    }
	    
	    advertsService.deleteAiring(airing);
	    return ResponseEntity.ok().build();
	}
    
    @GetMapping("/addAdvertStationMapping/{stations}/{advertId}")
    @CrossOrigin(origins = hostUrl)
	public ResponseEntity<Response> addAdvertStationMapping(@PathVariable(value = "stations") String stations, @PathVariable(value = "advertId") String advertId) throws JsonParseException, JsonMappingException, IOException {
    	String[] stationList = stations.split("_");
    	List<AdvertStationMapping> mappings = new ArrayList<AdvertStationMapping>();
    	
    	for (String station : stationList) {
    		AdvertStationMapping mapping = new AdvertStationMapping();
    		mapping.setAdvertId(new Long(advertId));
    		mapping.setStationId(new Long(station));
    		mapping.setDateCreated(new Date());
        	mapping.setCreatedBy(new Long(1));
        	mappings.add(mapping);
    	}
    	advertsService.saveAdvertStationMapping(mappings);
		return ResponseEntity.ok(new Response("Mapping has been added successfully", "Success"));
	}
    
    @GetMapping("/advertsStationMapping/{stationId}")
	public List<AdvertStationMapping> findAdvertsMappedToStation(@PathVariable(value = "stationId") Long stationId) {
		return advertsService.findAdvertsMappedToStation(stationId);
	}
    
    @GetMapping("/stationAdvertMapping/{advertId}")
	public List<AdvertStationMapping> findStationsMappedToAdvert(@PathVariable(value = "advertId") Long advertId) {
		return advertsService.findStationsMappedToAdvert(advertId);
	}
}